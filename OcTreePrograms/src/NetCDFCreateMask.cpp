#include <iostream>
#include <string>
#include <netcdf>

#include <Argv.h>
#include <filename_functions.h>

using namespace std;
using namespace netCDF;

int main(int argc, char* argv[])
{
  Argv args(argc, argv);
  NcFile existing;
  NcFile maskFile;

  existing.open(args[1], NcFile::read);

  string maskFileName = "COGS Mask File";


  maskFile.open(maskFileName + ".nc", NcFile::newFile, NcFile::nc4);
  maskFile.putAtt("description", NcType(ncChar), maskFileName.size(), maskFileName.c_str());

  auto atts = existing.getAtts();

  for (auto it = atts.begin(); it != atts.end(); it++) {
    if ((it->first.compare("time") != 0) && (it->first.compare("date") != 0)) {
      char * data;
      data = new char[it->second.getAttLength()];
      it->second.getValues(data);
      maskFile.putAtt(it->second.getName(), it->second.getType(), it->second.getAttLength(), data);
      delete[] data;
    }
  }

  auto dims = existing.getDims();
  for (auto it = dims.begin(); it != dims.end(); it++) {
    if (it->first.compare("time") != 0) {
      maskFile.addDim(it->first, it->second.getSize());
    }
  }

  auto vars = existing.getVars();
  for (auto varit = vars.begin(); varit != vars.end(); varit++) {
    if (varit->first.compare("time") != 0) {
      auto vardims = varit->second.getDims();

      vector<string> dimnames;
      for (auto dimit = vardims.begin(); dimit != vardims.end(); dimit++) {
        if (varit->first.compare("cloud") != 0 || dimit->getName().compare("time") != 0) {
          dimnames.push_back(dimit->getName());
        }
      }

      NcVar newvar;
      if (varit->first.compare("cloud") == 0) {
        newvar = maskFile.addVar("mask", varit->second.getType().getName(), dimnames);
        string descValue = "mask value, 0 for No Mask, -1 for Mask";
        newvar.putAtt("description", NcType(ncChar), descValue.size(), descValue.c_str());
        size_t xmax = existing.getDim("x").getSize();
        size_t ymax = existing.getDim("y").getSize();
        size_t zmax = existing.getDim("z").getSize();

        short * values = new short[xmax * ymax * zmax];

        vector<size_t> start, count;
        start.push_back(0); count.push_back(1);
        start.push_back(0); count.push_back(zmax);
        start.push_back(0); count.push_back(ymax);
        start.push_back(0); count.push_back(xmax);

        varit->second.getVar(start, count, values);
        for (int i = 0; i < (xmax*ymax*zmax); i++) {
          if (values[i] != -1) {
            values[i] = 0;
          }
        }
        newvar.putVar(values);
        delete[] values;
      } else {
        newvar = maskFile.addVar(varit->first, varit->second.getType().getName(), dimnames);

        auto varatts = varit->second.getAtts();
        for (auto attit = varatts.begin(); attit != varatts.end(); attit++) {
          char * data = new char[attit->second.getAttLength()];
          attit->second.getValues(data);
          newvar.putAtt(attit->first, attit->second.getType(), attit->second.getAttLength(), data);
          delete[] data;
        }
        int * values = new int[newvar.getDim(0).getSize()];
        varit->second.getVar(values);
        newvar.putVar(values);
        delete[] values;
      }
    }
  }

	maskFile.close();
	existing.close();
}