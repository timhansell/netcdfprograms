#include <iostream>
#include <fstream>
#include <string>
#include <netcdf>
#include <map>
#include <algorithm>
#include <Argv.h>
#include <filename_functions.h>

using namespace std;
using namespace netCDF;

int main(int argc, char* argv[])
{
  Argv args(argc, argv);
  NcFile COGS;
  int time_inc = 0;
  COGS.open(args[1], NcFile::read);
  if (args.size() > 2) {
    time_inc = stoi(args[2]);
  }

  NcDim xDimension = COGS.getDim("x");
  NcDim yDimension = COGS.getDim("y");
  NcDim zDimension = COGS.getDim("z");
  NcDim timeDimension = COGS.getDim("time");
  NcGroupAtt dateStamp = COGS.getAtt("date");
  char * dateString = new char[dateStamp.getAttLength() + 1];
  dateStamp.getValues(dateString);
  dateString[dateStamp.getAttLength()] = '\0';

  ofstream csv_file;
  csv_file.open(string(dateString) + ".csv");

  NcVar time = COGS.getVar("time");
  vector<size_t> tdim;
  tdim.push_back(0);

  size_t xmax = xDimension.getSize();
  size_t ymax = yDimension.getSize();
  size_t zmax = zDimension.getSize();
  size_t tmax = timeDimension.getSize();

  NcVar cloud = COGS.getVar("cloud");

  vector<size_t> address, count;
  address.push_back(0); count.push_back(1);
  address.push_back(0); count.push_back(1);
  address.push_back(0); count.push_back(ymax);
  address.push_back(0); count.push_back(xmax);
  
  short * data;

  data = new short[ymax * xmax];
  bool min_found = false;
  bool max_found = false;

  string year = string(dateString, (size_t)4);
  string month = string(dateString + 4, (size_t)2);
  string day = string(dateString + 6, (size_t)2);
  csv_file << "Time, Bottom, Top, Thickness" << endl;

  NcVar zvar = COGS.getVar("z");
  vector<size_t> zdim;
  zdim.push_back(0);
  int previous_time;

  for (size_t time_index = 0; time_index < tmax; time_index++) {
    int timestep;
    tdim[0] = time_index;
    time.getVar(tdim, &timestep);
    if (time_inc > 0) {
      if ((time_index > 0) && (timestep - time_inc > previous_time)) {
        time_index--;
        timestep = previous_time + time_inc;
      }
    }
    address[0] = time_index;
    cout << ".";
    csv_file << timestep << ", ";
    size_t min_cloud, max_cloud;
    min_found = max_found = false;
    for (size_t z_index = 0; z_index < zmax; z_index++) {
      address[1] = z_index;
      cloud.getVar(address, count, data);
      for (int i = 0; i < ymax*zmax; i++) {
        if (data[i] == 1) {
          if (!min_found) {
            min_cloud = z_index;
            min_found = true;
          } else {
            min_cloud = min(min_cloud, z_index);
          }
          if (!max_found) {
            max_found = true;
            max_cloud = z_index;
          } else {
            max_cloud = max(max_cloud, z_index);
          }
          break;
        }
      }
    }
    int minAlt, maxAlt, altDiff;
    zdim.push_back(0);
    if (min_found) {
      zdim[0] = min_cloud;
      zvar.getVar(zdim, &minAlt);
      zdim[0] = max_cloud;
      zvar.getVar(zdim, &maxAlt);
      altDiff = maxAlt - minAlt;
      csv_file << minAlt << ", " << maxAlt << ", " << altDiff << endl;
    } else {
      csv_file << "N/A, N/A, N/A" << endl;
    }
    previous_time = timestep;
  }
  COGS.close();
}
