#include <iostream>
#include <fstream>
#include <string>
#include <netcdf>
#include <octomap/octomap.h>
#include <octomap/OcTree.h>
#include <Argv.h>
#include <filename_functions.h>

using namespace std;
using namespace netCDF;

string zeroPad(int val, size_t len) {
  string answer;
  if (val < 0) {
    answer = "-" + zeroPad(-val, len - 1);
  } else {
    answer = to_string(val);
    while (answer.size() < len) {
      answer = "0" + answer;
    }
  }
  return answer;
}

int main(int argc, char* argv[])
{

	Argv args(argc, argv);
	NcFile existing;
  
  int time_inc = 0;

	existing.open(args[1],NcFile::read);
  if (args.size() > 2) {
    time_inc = stoi(args[2]);
  }
  NcDim xDimension = existing.getDim("x");
  NcDim yDimension = existing.getDim("y");
  NcDim zDimension = existing.getDim("z");
  NcDim timeDimension = existing.getDim("time");

  NcVar time = existing.getVar("time");
  NcVar cloud = existing.getVar("cloud");
  NcVar xVar = existing.getVar("x");
  NcVar yVar = existing.getVar("y");
  NcVar zVar = existing.getVar("z");

  size_t xmax = xDimension.getSize();
  size_t ymax = yDimension.getSize();
  size_t zmax = zDimension.getSize();
  size_t tmax = timeDimension.getSize();

  int maxTime, minTime;
  short * data = new short[xmax * ymax * zmax];
  
  vector<size_t> start;
  vector<size_t> count;
  
  start.push_back(0); count.push_back(1);
  start.push_back(0); count.push_back(zmax);
  start.push_back(0); count.push_back(ymax);
  start.push_back(0); count.push_back(xmax);

  vector<size_t> timeDimensionVector;
  timeDimensionVector.push_back(0);
  time.getVar(timeDimensionVector, &minTime);
  timeDimensionVector[0] = tmax - 1;
  time.getVar(timeDimensionVector, &maxTime);

  int previous_time;
  int stepcount = 0;

  for (size_t time_index = 0; time_index < tmax; time_index++) {

    octomap::OcTree tree_slice(50.0);

    timeDimensionVector[0] = time_index;
    int timestep;
    time.getVar(timeDimensionVector, &timestep);

    if (time_inc > 0) {
      if ((time_index > 0) && (timestep - time_inc > previous_time)) {
        time_index--;
        timestep = previous_time + time_inc;
      }
    }

		cout << "Processing timestamp [" << timestep << "]" << endl;
		string treeFile = BaseName(args[1], true) + "-" + zeroPad(stepcount, 3) + ".ot";

    start[0] = time_index;
    cloud.getVar(start, count, data);

    vector<size_t> dim; dim.push_back(0);

    double xval, yval, zval;

		for (size_t zi = 0; zi < zmax; zi++) {
      dim[0] = zi;
      zVar.getVar(dim, &zval);
      for (size_t yi = 0; yi < ymax; yi++) {
        dim[0] = yi;
        yVar.getVar(dim, &yval);
				for (size_t xi = 0; xi < xmax; xi++) {
          dim[0] = xi;
          xVar.getVar(dim, &xval);
          if (data[xi + (yi + zi * ymax) * xmax] == 1) {
            tree_slice.updateNode(xval, yval, zval,true);
					} 
				}
			}
		}
    ofstream Oc_Out;
    Oc_Out.open(treeFile, ofstream::out | ofstream::binary);
    Oc_Out << "# Octomap OcTree file" << endl
      << "# (feel free to add / change comments, but leave the first line as it is!)" << endl
      << "#" << endl
      << "# timestamp = " << timestep << endl
      << "#" << endl
      << "id " << tree_slice.getTreeType() << endl
      << "size " << tree_slice.size() << endl
      << "res " << tree_slice.getResolution() << endl
      << "data" << endl;

    tree_slice.writeData(Oc_Out);
    Oc_Out.close();
    // tree_slice.write(treeFile);
    
    previous_time = timestep;
    stepcount++;
	}
	existing.close();
}
