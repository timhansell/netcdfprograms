#include <iostream>
#include <string>
#include <netcdf>
#include <map>

#include <Argv.h>
#include <filename_functions.h>

using namespace std;
using namespace netCDF;

int main(int argc, char* argv[])
{
	Argv args(argc, argv);
	NcFile existing;
  NcFile created;
  
  int time_inc = 0;

	existing.open(args[1], NcFile::read);
  if (args.size() > 2) {
    time_inc = stoi(args[2]);
  } 

  NcDim xDimension = existing.getDim("x");
  NcDim yDimension = existing.getDim("y");
  NcDim zDimension = existing.getDim("z");
  NcDim timeDimension = existing.getDim("time");

  NcVar time = existing.getVar("time");

  size_t xmax = xDimension.getSize();
  size_t ymax = yDimension.getSize();
  size_t zmax = zDimension.getSize();
  size_t tmax = timeDimension.getSize();
  int maxTime, minTime;

  vector<size_t> timeDimensionVector;
  timeDimensionVector.push_back(0);
  time.getVar(timeDimensionVector, &minTime);
  timeDimensionVector[0] = tmax-1;
  time.getVar(timeDimensionVector, &maxTime);
  
  int previous_time;

  for (size_t time_index = 0; time_index < tmax; time_index++) {
    timeDimensionVector[0] = time_index;
    int timestep;
    existing.getVar("time").getVar(timeDimensionVector, &timestep);
    if (time_inc > 0) {
      if ((time_index >  0) && (timestep - time_inc > previous_time)) {
        time_index--;
        timestep = previous_time + time_inc;
      } 
    }

    string timestamp = to_string(timestep);

    string createdFileName = BaseName(argv[1], true) + "-" + timestamp + ".nc";
    cout << "Creating file " << createdFileName << " for time step " << timestep << endl;
		created.open(createdFileName,NcFile::newFile, NcFile::nc4);

    created.putAtt("timestamp",NcType(ncChar),timestamp.size(), timestamp.c_str());

    auto atts = existing.getAtts();

    for (auto it = atts.begin(); it != atts.end(); it++) {
      char * data;
      data = new char[it->second.getAttLength()];
      it->second.getValues(data);
      created.putAtt(it->second.getName(), it->second.getType(), it->second.getAttLength(), data);
      delete[] data;
    }

    auto dims = existing.getDims();
    for (auto it = dims.begin(); it != dims.end(); it++) {
      if (it->first.compare("time") != 0) {
        created.addDim(it->first, it->second.getSize());
      }
    }

    vector<size_t> start;
    vector<size_t> count;
    start.push_back(0); count.push_back(1);     // time
    start.push_back(0); count.push_back(zmax);  // z
    start.push_back(0); count.push_back(ymax);  // y
    start.push_back(0); count.push_back(xmax);  // x

    auto vars = existing.getVars();
    for (auto varit = vars.begin(); varit != vars.end(); varit++) {
      if (varit->first.compare("time") != 0) {
        auto vardims = varit->second.getDims();
        vector<string> dimnames;
        for (auto dimit = vardims.begin(); dimit != vardims.end(); dimit++) {
          if (varit->first.compare("cloud") != 0 || dimit->getName().compare("time") != 0) {
            dimnames.push_back(dimit->getName());
          }
        }
        auto newvar = created.addVar(varit->first, varit->second.getType().getName(), dimnames);
        auto varatts = varit->second.getAtts();
        for (auto attit = varatts.begin(); attit != varatts.end(); attit++) {
          char * data = new char[attit->second.getAttLength()];
          attit->second.getValues(data);
          newvar.putAtt(attit->first, attit->second.getType(), attit->second.getAttLength(), data);
        }

        if (varit->first.compare("cloud") == 0) {
          short * values = new short[xmax * ymax * zmax];
          start[0] = time_index;
          varit->second.getVar(start, count, values);
          newvar.putVar(values);
          delete[] values;
        } else {
          int * values = new int[newvar.getDim(0).getSize()];
          varit->second.getVar(values);
          newvar.putVar(values);
          delete[] values;
        }
        previous_time = timestep;
      }
    }

		created.close(); 
	}
	existing.close();
}
