#ifndef COMMAND_LINE_ARGUMENTS_H
#define COMMAND_LINE_ARGUMENTS_H

#include <string>
#include <vector>
class CommandLineArguments
{
private:
  std::string program_name;
  std::vector<std::string> args;
public:
  CommandLineArguments();
  CommandLineArguments(int argc, char * argv[]);
  ~CommandLineArguments();

  size_t process(int argc, char* argv[]);
};

#endif // COMMAND_LINE_ARGUMENTS_H