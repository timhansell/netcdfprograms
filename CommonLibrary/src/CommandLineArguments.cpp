#include "CommandLineArguments.h"

CommandLineArguments::CommandLineArguments() {
  program_name = "unknown";
}

CommandLineArguments::CommandLineArguments(int argc, char * argv[]) {
  process(argc, argv);
}

CommandLineArguments::~CommandLineArguments() {}

size_t CommandLineArguments::process(int argc, char * argv[]) {
  program_name = argv[0];
  for (int i = 1; i < argc; i++) {
    args.push_back(argv[i]);
  }
  return args.size();
}